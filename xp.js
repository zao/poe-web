function perc(plvl, alvl) {
	var l5 = plvl+5;
	var safe = Math.floor(plvl/16);
	var d = Math.abs(plvl - alvl);
	var ed = Math.max(0, d-3-safe);
	var rate = Math.pow(l5 / (l5 + Math.pow(ed, 2.5)), 1.5);
	if (plvl >= 95) {
		rate /= 1 + 0.1 * (plvl - 94);
	}
	return Math.max(rate, 0.01);
}

function compute_level_table(level) {
	var data = [];
	var cutoff = 5;
	var top_maps = {
		77: 76.9,
		78: 77.7,
		79: 78.4,
		80: 79.0,
		81: 79.5,
		82: 79.9 };
	for (var map_level = 1; map_level <= 82; ++map_level) {
		var adj_level = top_maps[map_level] ? top_maps[map_level] : map_level;
		var amount = perc(level, adj_level);
		var n = amount * 100;
		if (n >= cutoff || (level >= 60 && map_level >= 66)) {
			var name = map_level >= 68 ? "T" + (map_level - 67) : map_level;
			data.push({name: name.toString(), value: n});
		}
	}
	return data;
}

function set_graph(level) {
	var data = compute_level_table(level);

	var margin = { top: 20, right: 30, bottom: 30, left: 40},
	    width = 900 - margin.left - margin.right,
	    height = 500 - margin.top - margin.bottom;

	var x = d3.scale.ordinal()
		.rangeRoundBands([0, width], .1);

	var y = d3.scale.linear()
		.range([height, 0]);

	x.domain(data.map(function (d) { return d.name; }));
	y.domain([0, d3.max(data, function (d) { return d.value; })]);

	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom");

	var yAxis = d3.svg.axis()
		.scale(y)
		.orient("left");

	var parent = d3.select("#graph-set");
	parent.html("");

	var chart = parent.append('svg')
		.classed("chart", true)
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");


	chart.append("g")
		.attr("class", "x axis")
		.attr("transform", "translate(0," + height + ")")
		.call(xAxis);

	chart.append("g")
		.attr("class", "y axis")
		.call(yAxis);

	var barWidth = width / data.length;

	var bar = chart.selectAll(".bar")
		.data(data);

	bar.enter().append("rect")
		.attr("class", "bar")
		.attr("x", function (d) { return x(d.name); })
		.attr("y", function (d) { return y(d.value); })
		.attr("height", function (d) { return height - y(d.value); })
		.attr("width", x.rangeBand());

	bar.enter().append("text")
		.attr("class", "bar")
		.attr("x", function (d) { return x(d.name) + x.rangeBand() / 2; })
		.attr("y", function (d) { return y(d.value) + 3; })
		.attr("dy", ".75em")
		.text(function (d) { return d.value.toFixed(0) + "%"; });
}

/*
angular.module('expRateApp', [])
	.controller('ExpRateController', ['$scope', function($scope) {
		$scope.rates = [];
		$scope.level = function () {
			var plvl = parseInt($scope.plvl, 10);
			$scope.rates = [];
			if (isNaN(plvl)) { return; }
			var ed = 3 + Math.floor(plvl/16);
			var lo = plvl - ed;
			var hi = plvl + ed;
			for (var i = Math.max(lo - 15, 1); i < lo; ++i) {
				$scope.rates.push({level: i, rate: perc(plvl, i)});
			}
			$scope.rates.push({level: lo + " - " + hi, rate: 1.0 });
			for (var i = hi+1; i <= Math.min(hi + 15, 100); ++i) {
				$scope.rates.push({level: i, rate: perc(plvl, i)});
			}
			console.log($scope.rates);
		};
	}])
	.filter('percent', function() {
		return function(input, param) {
			return (input * 100.0).toFixed(param) + "%";
		}
	});
*/
